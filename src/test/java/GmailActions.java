import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailActions {

    @FindBy(xpath = "//input[@id='identifierId']")
    private WebElement inputLogin;

    @FindBy(id ="identifierNext")
    private WebElement buttonSendEmailAddress;

    @FindBy(css = "input[type='password']:nth-child(1)")
    private WebElement inputPassword;

    @FindBy(id = "passwordNext")
    private WebElement buttonSendPassword;

    @FindBy(xpath = "//div[text()='Написати']")
    private WebElement buttonCompose;

    @FindBy(className = "vO")
    private WebElement inputAddress;

    @FindBy(className = "aoT")
    private WebElement inputSubject;

    @FindBy(id=":a3")
    private WebElement inputMessage;

    @FindBy(className="Ha")
    private WebElement closeEmail;

    @FindBy(xpath = "//div[@class='TN bzz aHS-bnq']") //className="TN bzz aHS-bnq") //id = ":46")  //xpath = "//div#[@class='TO' and @id=':46']") //className="TN bzz aHS-bnq")
    private WebElement draftOpen;

    @FindBy(className = "zA yO")
    private WebElement openDraftEmail;

    @FindBy(className = "T-I J-J5-Ji aoO T-I-atl L3")
    private WebElement buttonSendEmail;

    WebDriver driver;

    public GmailActions(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver=driver;
    }

    public void enterLoginAndSubmit(String email){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(inputLogin)).sendKeys(email);
//        inputLogin.sendKeys(email);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonSendEmailAddress)).click();
//        buttonSendEmailAddress.click();
    }

    public void enterPasswordAndSubmit(String password){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(inputPassword)).sendKeys(password);
//        inputPassword.sendKeys(password);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonSendPassword)).click();
//        buttonSendPassword.click();
    }

    public void openFillAndCloseEmail(String address, String subject, String message){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonCompose)).click();
//        buttonCompose.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions. visibilityOf(inputAddress)).sendKeys(address);
//        inputAddress.sendKeys(address);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(inputSubject)).sendKeys(subject);
//        inputSubject.sendKeys(subject);
//        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(inputMessage)).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(inputMessage)).sendKeys(message);
//        inputMessage.sendKeys(message);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(closeEmail)).click();
//        closeEmail.click();
    }

    public void openDraftAndSendEmail(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(draftOpen)).click();
//        draftOpen.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(openDraftEmail)).click();
//        openDraftEmail.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonSendEmail)).click();
//        buttonSendEmail.click();
    }




}
